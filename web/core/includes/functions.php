<?php

/**
 * Funcion de autocarga de clases, permite la autocarga de controladores y modelos
 */
function __autoload($name)
{
    $file = './model/' . $name . '.php';
    if (file_exists($file))
        require_once($file);

    $file = './controller/'. $name . ('.php');
    if (file_exists($file))
        require_once($file);
}

/**
 * Genera una semilla aleatoria para la encritacion simetrica
 */
function randomSalt(){
    // A higher "cost" is more secure but consumes more processing power
    $cost = 10;

    // Create a random salt
    $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
    // Prefix information about the hash so PHP knows how to verify it later.
    // "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
    return sprintf("$2a$%02d$", $cost) . $salt;
}

/**
 * Encripta una pass con la semilla definida en conf.php
 */
function cryptPass($pass){
    return crypt($pass, $GLOBALS['SALT']);
}
