<?php

/**
 * Define los lugares publicos del sitio web
 */
$GLOBALS['public_place'] = array(
    'User:login',
    'User:',
    'Controller:jcryption',
);

/**
 * Define La configuración de conexion a la base de datos
 */
$conf = parse_ini_file( 'config/config.ini',true);

$GLOBALS['DB_CONF']= array(
    'DSN' => 'mysql:dbname='.$conf['DataBase']['name'].';host='.$conf['DataBase']['address'].'',
    'userDB' => $conf['DataBase']['user'],
    'passDB' => $conf['DataBase']['password'],
);

/**
 * Almacena la semilla que se usar para la encryptacion de las passwords.
 */
$GLOBALS['SALT'] = '$2a$10$R4W7fAv1cunfBWxsHC7kgQ==';
