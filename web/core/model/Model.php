<?php

class Model{

    protected $DB;
    
    function __construct(){
        try {
            $this->DB = new PDO($GLOBALS['DB_CONF']['DSN'], $GLOBALS['DB_CONF']['userDB'], $GLOBALS['DB_CONF']['passDB']);

        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
    }

    function beginTransaction(){
        $this->DB->beginTransaction();
    }
    function commit(){
        $this->DB->commit();
    }
    function rollback(){
        $this->DB->rollBack();
    }

    function __destruct (){
        if(!isset($this->DB)){
            unset($this->DB);
        }
    }
}