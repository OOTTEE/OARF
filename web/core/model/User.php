<?php

class User extends Model{
    public $id;
    public $name;
    public $password;
    public $user;
    public $date;
    public $admin;

    public function __construct($args=array()){
        parent::__construct();
        $this->id = isset($args['id'])? $args['id'] : null;
        $this->name = isset($args['name'])? $args['name'] : null;
        $this->password = isset($args['password'])? $args['password'] : null;
        $this->user = isset($args['user'])? $args['user'] : null;
        $this->date = isset($args['date'])? $args['date'] : null;
        $this->admin = isset($args['admin'])? $args['admin'] : null;
    }



}