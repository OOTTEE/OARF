<?php
/*
 * STATUS for header status http response.
 */
define('STATUS_OK', 200);
define('STATUS_CREATED', 201);
define('STATUS_NO_CONTENT', 204);
define('STATUS_NOT_MODIFIED', 304);
define('STATUS_BAD_REQUEST', 400);
define('STATUS_UNAUTHORIZED', 401);
define('STATUS_FORBIDDEN', 403);
define('STATUS_NOT_FOUND', 404);
define('STATUS_METHOD_NOT_ALLOWED', 405);
define('STATUS_GONE', 410);
define('STATUS_UNSOPORTTED_MEDIA_TYPE', 415);
define('STATUS_UNPROCESSABLE_ENTITY', 422);
define('STATUS_TOO_MANY_REQUEST', 429);
/*
 * ERRORS for controller response.
 */
define('ERROR_NO_CONTROLLER_DEFINED','0001');
define('ERROR_METHOD_NOT_ALLOWED','0002');
define('ERROR_ACTION_NOT_ALLOWED','0003');

class Controller {
    private $error = null;
    private $response = [];

    //METHODS
    function GET(){
        $this->setStatus(STATUS_NO_CONTENT);
    }
    function POST(){
        $this->setStatus(STATUS_UNAUTHORIZED);
    }
    function PUT(){
        $this->setStatus(STATUS_UNAUTHORIZED);
    }
    function DELETE(){
        $this->setStatus(STATUS_UNAUTHORIZED);
    }
    function PATCH(){
        $this->setStatus(STATUS_UNAUTHORIZED);
    }
    function VERSION(){
        $this->setResponse($this->getVersion());
    }

    //HELPER METHODS
    function notControllerFound(){
        $this->setError(ERROR_NO_CONTROLLER_DEFINED, 'ERROR_NO_CONTROLLER_DEFINED');
        $this->setStatus(STATUS_BAD_REQUEST);
    }
    function setStatus($status){
        http_response_code($status);
    }
    function getVersion(){
        return 'Version: beta 0.0.1';
    }
    function setError($code='0000' , $message="Error not defined", $args = null ){
        if ($this->error){
            array_push($this->error, [
                'code' => $code,
                'message' => $message,
                'args' => $args
            ]);
        }else{
            $this->error = [ [
                'code' => $code,
                'message' => $message,
                'args' => $args
            ]];
        }
    }

    function setResponse(...$args){
        $this->response = array_merge($this->response, $args);
    }

    function __destruct(){
        if (!is_null($this->error)){
            echo json_encode($this->error);
        }
        if (sizeof($this->response) > 0){
            echo json_encode($this->response);
        }

    }

}