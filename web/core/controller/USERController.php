<?php

class USERController extends Controller{

    function GET(){
        if( $this->id == 1)
            echo 'USER GET' . $this->id;
        else {
            $this->setError(0000, 'id falso');
            $this->setError(0001, 'otro', ['turu' => 'cierto']);
            $this->setStatus(STATUS_BAD_REQUEST);
        }
    }

    function POST(){
        echo 'USER POST';
    }
    function PUT(){
        echo 'USER PUT';
    }
    function DELETE(){
        echo 'USER DELETE';
    }
    function PATCH()    {
        echo 'USER PATCH';
    }
    function LOGIN(){
        $this->setResponse(true);
    }

}