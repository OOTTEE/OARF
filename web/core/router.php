<?php
require_once("includes/functions.php");
require_once("includes/conf.php");
require_once 'includes/sqAES.php';
require_once 'includes/jcryption.php';

class router{
    public static $METHODS = array('GET','POST','PUT','DELETE','PATCH');

    private $controller;
    private $method;
    private $action;

    function __construct(){
        session_start();
        $this->controller = isset($_REQUEST['controller']) ? $_REQUEST['controller'].'Controller' : 'Controller';
        $this->action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    function run(){
        if (class_exists($this->controller)) {
            $obj = new $this->controller();
            if ( in_array($this->method , router::$METHODS )){
                if(!is_null($this->action)) {
                    if (method_exists($obj, $this->action))
                        $obj->{$this->action}();
                    else {
                        $obj->setError(ERROR_ACTION_NOT_ALLOWED, 'ERROR_ACTION_NOT_ALLOWED');
                        $obj->setStatus(STATUS_BAD_REQUEST);
                    }
                }else{
                    $obj->{$this->method}();
                }
            }else{
                $obj->setError(ERROR_METHOD_NOT_ALLOWED, 'ERROR_METHOD_NOT_ALLOWED');
                $obj->setStatus(STATUS_METHOD_NOT_ALLOWED);

            }
        }else{
            $controller = new Controller();
            $controller->notControllerFound();
        }
    }

    function  __destruct(){
        session_write_close();
    }
}