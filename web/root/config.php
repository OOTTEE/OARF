<?php
    function write_ini_file($assoc_arr, $path, $has_sections=FALSE) {
        $content = "";
        if ($has_sections) {
            foreach ($assoc_arr as $key=>$elem) {
                $content .= "[".$key."]\n";
                foreach ($elem as $key2=>$elem2) {
                    if(is_array($elem2))
                    {
                        for($i=0;$i<count($elem2);$i++)
                        {
                            $content .= $key2."[] = ".$elem2[$i]."\n";
                        }
                    }
                    else if($elem2=="") $content .= $key2." = \n";
                    else $content .= $key2." = ".$elem2."\n";
                }
            }
        }
        else {
            foreach ($assoc_arr as $key=>$elem) {
                if(is_array($elem))
                {
                    for($i=0;$i<count($elem);$i++)
                    {
                        $content .= $key."[] = ".$elem[$i]."\n";
                    }
                }
                else if($elem=="") $content .= $key." = \n";
                else $content .= $key." = ".$elem."\n";
            }
        }

        if (!$handle = fopen($path, 'w')) {
            return false;
        }

        $success = fwrite($handle, $content);
        fclose($handle);

        return $success;
    }

    $ERROR = 'NO';
    if( !file_exists('../server/config.ini')){
        if(isset($_POST['config'])) {
            if (isset($_POST['DB']) AND $_POST['DB'] != '' AND
                isset($_POST['pass']) AND $_POST['pass'] != '' AND
                isset($_POST['User']) AND $_POST['User'] != '' AND
                isset($_POST['channel'])
            ) {
                $dsn = 'mysql:dbname=' . $_POST['DB'] . ';host=127.0.0.1';
                try {
                    $DB = new PDO($dsn, $_POST['User'], $_POST['pass']);
                    $DB_CONNECTION = true;
                } catch (PDOException $e) {
                    echo 'Falló la conexión: ' . $e->getMessage();
                }
                if ($DB_CONNECTION) {
                    $CONFIG = array(
                        'DataBase' => array(
                            'address' => '127.0.0.1',
                            'port' => '3306',
                            'user' => $_POST['User'],
                            'name' => $_POST['DB'],
                            'password' => $_POST['pass'],
                        ),
                        'Radio' => array(
                            'channel' => $_POST['channel'],
                            'power' => 'MAX',
                        ));
                    write_ini_file($CONFIG, '../server/config.ini', true);
                    header('Location: /');
                } else {
                    $ERROR = 'DB';
                }
            } else {
                $ERROR = 'FIELDS';
            }
        }
    }else{
        header('Location: /');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Instalador GAHF</title>
    <!-- Bootstrap core CSS -->
    <link href="/public/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/lib/bootstrap/css/bootstrap-switch.min.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <?php if($ERROR == 'FIELDS'): ?>
        <div class="alert alert-warning" >
            Complete todos los campos.
        </div>
    <?php endif; ?>
    <?php if($ERROR == 'DB'): ?>
        <div class="alert alert-warning" >
            Los datos de conexion a la base de datos son incorrectos.
        </div>
    <?php endif; ?>
    <div class="jumbotron">
        <form action="config.php" method="post" >
            <div class="well">
                Bienvenido al instalador de GAHF, a continuación se le solicitarán datos de conexión a la base de datos.

                <div class="form-group">
                    <label for="BD">Nombre de la base de datos</label>
                    <input type="text" class="form-control" id="DB" name="DB" placeholder="Base de datos" <?= (isset($_POST['DB']))? 'value="'.$_POST['DB'].'"' : '' ?>>
                </div>
                <div class="form-group">
                    <label for="usuario">Usuario</label>
                    <input type="text" class="form-control" id="usuario" name="User" placeholder="User" <?= (isset($_POST['User']))? 'value="'.$_POST['User'].'"' : '' ?>>
                </div>
                <div class="form-group">
                    <label for="pass">Contraseña</label>
                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
                </div>
            </div>
            <div class="well">
                A continuación se solicita la banda de funcionamiento del sistema de comunicaciones inalambricas.
                <div class="form-group">
                    <label for="channel">Canal</label>
                    <select class="form-control" name="channel" id="channel">
                        <?php $i = 50; while ($i<= 150):?>
                            <option <?= (isset($_POST['channel']) AND $_POST['channel'] == $i )? 'selected' : '' ?> ><?= $i ?></option>
                        <?php $i++; endwhile; ?>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-default" name="config" value="1">Continuar</button>
        </form>
    </div>
</div>
<script src="/public/lib/js/jquery.min.js"></script>
<script src="/public/lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

